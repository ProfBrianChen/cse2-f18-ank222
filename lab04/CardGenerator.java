//Andrew Kelly
//CSE02
//9/21/18

/*This program will:
generate a random number 1-52,
and output the identity and suit based on this integer*/

import java.lang.Math;  //Imports the math class

public class CardGenerator {    //Creates and names the class CardGenerator
  
  public static void main(String[] args) {    //Starts the main method needed for every java program
    
    System.out.println("Welcome to the Random Card Generator!");    //Outputs greeting
    
    int cardNumber = (int)(Math.random()*52)+1;    //generates random number 1-52
   
    if (cardNumber == 1) {   //Assigns output for when 1 is generated
      System.out.println("You picked the Ace of Diamonds");    //Outputs identity and suit for number
    }   //ends the if statement
    else if (cardNumber == 2) {   //Assigns output for when 2 is generated
      System.out.println("You picked the 2 of Diamonds");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 3) {   //Assigns output for when 3 is generated
      System.out.println("You picked the 3 of Diamonds");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 4) {   //Assigns output for when 4 is generated
      System.out.println("You picked the 4 of Diamonds");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 5) {   //Assigns output for when 5 is generated
      System.out.println("You picked the 5 of Diamonds");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 6) {   //Assigns output for when 6 is generated
      System.out.println("You picked the 6 of Diamonds");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 7) {   //Assigns output for when 7 is generated
      System.out.println("You picked the 7 of Diamonds");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 8) {   //Assigns output for when 8 is generated
      System.out.println("You picked the 8 of Diamonds");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 9) {   //Assigns output for when 9 is generated
      System.out.println("You picked the 9 of Diamonds");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 10) {   //Assigns output for when 10 is generated
      System.out.println("You picked the 10 of Diamonds");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 11) {   //Assigns output for when 11 is generated
      System.out.println("You picked the Jack of Diamonds");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 12) {   //Assigns output for when 12 is generated
      System.out.println("You picked the Queen of Diamonds");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 13) {   //Assigns output for when 13 is generated
      System.out.println("You picked the King of Diamonds");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 14) {   //Assigns output for when 14 is generated
      System.out.println("You picked the Ace of Clubs");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 15) {   //Assigns output for when 15 is generated
      System.out.println("You picked the 2 of Clubs");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 16) {   //Assigns output for when 16 is generated
      System.out.println("You picked the 3 of Clubs");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 17) {   //Assigns output for when 17 is generated
      System.out.println("You picked the 4 of Clubs");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 18) {   //Assigns output for when 18 is generated
      System.out.println("You picked the 5 of Clubs");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 19) {   //Assigns output for when 19 is generated
      System.out.println("You picked the 6 of Clubs");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 20) {   //Assigns output for when 20 is generated
      System.out.println("You picked the 7 of Clubs");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 21) {   //Assigns output for when 21 is generated
      System.out.println("You picked the 8 of Clubs");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 22) {   //Assigns output for when 22 is generated
      System.out.println("You picked the 9 of Clubs");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 23) {   //Assigns output for when 23 is generated
      System.out.println("You picked the 10 of Clubs");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 24) {   //Assigns output for when 24 is generated
      System.out.println("You picked the Jack of Clubs");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 25) {   //Assigns output for when 25 is generated
      System.out.println("You picked the Queen of Clubs");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 26) {   //Assigns output for when 26 is generated
      System.out.println("You picked the King of Clubs");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 27) {   //Assigns output for when 27 is generated
      System.out.println("You picked the Ace of Hearts");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 28) {   //Assigns output for when 28 is generated
      System.out.println("You picked the 2 of Hearts");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 29) {   //Assigns output for when 29 is generated
      System.out.println("You picked the 3 of Hearts");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 30) {   //Assigns output for when 30 is generated
      System.out.println("You picked the 4 of Hearts");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 31) {   //Assigns output for when 31 is generated
      System.out.println("You picked the 5 of Hearts");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 32) {   //Assigns output for when 32 is generated
      System.out.println("You picked the 6 of Hearts");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 33) {   //Assigns output for when 33 is generated
      System.out.println("You picked the 7 of Hearts");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 34) {   //Assigns output for when 34 is generated
      System.out.println("You picked the 8 of Hearts");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 35) {   //Assigns output for when 35 is generated
      System.out.println("You picked the 9 of Hearts");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 36) {   //Assigns output for when 36 is generated
      System.out.println("You picked the 10 of Hearts");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 37) {   //Assigns output for when 37 is generated
      System.out.println("You picked the Jack of Hearts");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 38) {   //Assigns output for when 38 is generated
      System.out.println("You picked the Queen of Hearts");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 39) {   //Assigns output for when 39 is generated
      System.out.println("You picked the King of Hearts");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 40) {   //Assigns output for when 40 is generated
      System.out.println("You picked the Ace of Spades");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 41) {   //Assigns output for when 41 is generated
      System.out.println("You picked the 2 of Spades");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 42) {   //Assigns output for when 42 is generated
      System.out.println("You picked the 3 of Spades");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 43) {   //Assigns output for when 43 is generated
      System.out.println("You picked the 4 of Spades");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 44) {   //Assigns output for when 44 is generated
      System.out.println("You picked the 5 of Spades");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 45) {   //Assigns output for when 45 is generated
      System.out.println("You picked the 6 of Spades");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 46) {   //Assigns output for when 46 is generated
      System.out.println("You picked the 7 of Spades");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 47) {   //Assigns output for when 47 is generated
      System.out.println("You picked the 8 of Spades");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 48) {   //Assigns output for when 48 is generated
      System.out.println("You picked the 9 of Spades");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 49) {   //Assigns output for when 49 is generated
      System.out.println("You picked the 10 of Spades");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 50) {   //Assigns output for when 50 is generated
      System.out.println("You picked the Jack of Spades");    //Outputs identity and suit for number
    }   //ends the else f statement
    else if (cardNumber == 51) {   //Assigns output for when 51 is generated
      System.out.println("You picked the Queen of Spades");    //Outputs identity and suit for number
    }   //ends the else if statement
    else if (cardNumber == 52) {   //Assigns output for when 52 is generated
      System.out.println("You picked the King of Spades");    //Outputs identity and suit for number
    }   //ends the else if statement
    
    System.out.println("Thanks for playing, have a great day!");    //Outputs a closing statement
    
    
  }   //Ends the main method
}     //Ends the class statement