import java.lang.Math;  //Imports the math class

public class test {    //Creates and names the class CardGenerator
  
  public static void main(String[] args) {    //Starts the main method needed for every java program
    
    System.out.println("Welcome to the Random Card Generator!");    //Outputs greeting
    
    int cardNumber = (int)(Math.random()*3)+1;    //generates random number 1-52
   
    if (cardNumber == 1) {   //Assigns output for when 1 is generated
      System.out.println("You picked the Ace of Diamonds");    //Outputs identity and suit for number
    }   //ends the if statement
    else if (cardNumber == 2) {   //Assigns output for when 2 is generated
      System.out.println("You picked the 2 of Diamonds");    //Outputs identity and suit for number
       }   //ends the else if statement
    else if (cardNumber == 3) {   //Assigns output for when 3 is generated
      System.out.println("You picked the 3 of Diamonds");    //Outputs identity and suit for number
    }   //ends the else if statement
   
  }
}
