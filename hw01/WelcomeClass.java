 // Andrew Kelly
 // 8/31/2018
 // CSE 02 
//Above is my intro, states name, date and class
 
 public class WelcomeClass{ // starts code
 
  public static void main(String args[]){ //starts string
  
    //Everything below is the actual coding, System.out.print("") is what causes the output to register, adding ln after print creates a new line// 
    System.out.println("  -----------  ");
    System.out.println("  | WELCOME |  ");
    System.out.println("  -----------  ");
    System.out.println("  ^  ^  ^  ^  ^  ^  ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-A--N--K--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v  ");
    System.out.println("Welcome Class, my name is Andy Kelly and I am a freshman at Lehigh.");
    System.out.println("I used to live outside of Philadelphia and now I live in Upper Cents.");
    System.out.println("Although I have no prior coding expierence, I find it fascinating.");
    System.out.println("That being said, the backslashes gave me a lot of trouble!");                   
  
    //Lines 15 and 17 encountered a great deal of trouble because a single backslash produces an illegal escape error. 
    //To counteract this and push the single backslash throught to output I had to add the double backslash to confirm that I really wanted that.
    // Lines 19 through 22 are my "tweet lenth" bibliography, per instruction.
  }
 }

// The curly brackets at the end each coincide with the curly brackets up top, the code would not without them.