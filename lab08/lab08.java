//Andrew Kelly
//CSE02
//11.9.18

/* This code will create 2 arrays. The first will be filled with random digits,
the second will tell the user how many occurences of digits there are.*/

import java.util.*;    //imports utils
  
public class lab08 {    //creates class lab08
  public static void main (String [] args){   //creates main method
    int i = 0;
    int [] arrayRandom;
    arrayRandom = new int [100];
    
    for (i = 0; i < arrayRandom.length; i++){
      arrayRandom[i] = (int) (Math.random()*100);
      System.out.print(arrayRandom[i] + " ");
    }
    int [] arrayTwo;
    arrayTwo = new int [100];
    for(i=0; i < arrayRandom.length; i++){
      int occurence = arrayRandom[i];
      arrayTwo[occurence]++;
    }
    System.out.println("");
    for(i=0; i < arrayTwo.length; i++){
      System.out.println(i + " occurs " + arrayTwo[i] + " times.");
    }

  } //ends main method
}   //ends class