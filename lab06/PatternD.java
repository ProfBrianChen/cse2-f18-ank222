//Andrew Kelly
//CSE02
//10.12.18

/*This code will allow the user to input an integer from 1-10
and will then output a pyramid based on that integer 
using a pattern of spaces. */

import java.util.Scanner;     //Imports scanner

public class PatternD {                         //Starts the class
  public static void main (String[] args){      //Main method needed for java
    
     Scanner myScanner;      //Declares scanner myScanner
        myScanner = new Scanner(System.in);     //Declares myScanner

    //The bottom three lines welcome and prompt the user
    System.out.println("Welcome to the pyramid creator!");
    System.out.println("This pyramid will follow a unique pattern based on the integer you input.");
    System.out.println("Please enter an integer now.");
   
    boolean continueLoop = myScanner.hasNextInt();      //creates boolean continueLoop, 23-28 check if integer
    while (!continueLoop){    //starts while for when continueloop is false
      myScanner.next();       //calls scanner
      System.out.println("integer check");    //Outputs that the user did not enter integer
      continueLoop = myScanner.hasNextInt();      //sends user back through
    }     //closes while for integer check
    
    int userInt = myScanner.nextInt();      //declares int userInt through scanner, lines 30-34 deal with checking range
    while ((userInt <= 0) || (userInt >= 11)){    //while loop and checks range
      System.out.println("range check");    //outputs that the user did not enter within range
      userInt = myScanner.nextInt();      //sends user back through
    }     //closes while for range check
    
    System.out.println("Your pyramid is as follows");         //outputs ""
    for (int numRows = 1; numRows <= userInt; numRows++){     //outer loop for numRows
            for (int numCollumns = userInt; numCollumns >= numRows; numCollumns--){   //inner loop for numCollumns
                System.out.print(numCollumns + " ");    //outputs pattern
            }     //ends inner loop
            System.out.println();    //Takes the output to a new line, fulfills pattern
        }     //ends outer loop
    
   
  }   //ends the main method
}     //ends the class