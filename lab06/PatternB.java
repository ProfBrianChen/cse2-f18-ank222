//Andrew Kelly
//CSE02
//10.12.18

/*This code will allow the user to input an integer from 1-10
and will then output a pyramid based on that integer 
using a pattern of spaces. */

import java.util.Scanner;     //Imports scanner

public class PatternB {                         //Starts the class
  public static void main (String[] args){      //Main method needed for java
    
     Scanner myScanner;      //Declares scanner myScanner
        myScanner = new Scanner(System.in);     //Declares myScanner
                         
    //The bottom three lines welcome and prompt the user
    System.out.println("Welcome to the pyramid creator!");
    System.out.println("This pyramid will follow a unique pattern based on the integer you input.");
    System.out.println("Please enter an integer now.");
   
  boolean continueLoop = myScanner.hasNextInt();      //creates boolean continueLoop, 22-27 check if integer
    while (!continueLoop){    //starts while for when continueloop is false
      myScanner.next();       //calls scanner
      System.out.println("integer check");    //Outputs that the user did not enter integer
      continueLoop = myScanner.hasNextInt();      //sends user back through
    }     //closes while for integer check
    
    int userInt = myScanner.nextInt();      //declares int userInt through scanner, lines 29-33 deal with checking range
    while ((userInt <= 0) || (userInt >= 11)){    //while loop and checks range
      System.out.println("range check");    //outputs that the user did not enter within range
      userInt = myScanner.nextInt();      //sends user back through
    }     //closes while for range check
   
        
    System.out.println("Your pyramid is as follows");         //outputs ""
   
    int spaces = userInt;    //decalres new variable needed for pattern

        for(int numRows = spaces; numRows >= 1; --numRows) {  //outer loop for numRows
            for(int numCollumns = 1; numCollumns <= numRows; ++numCollumns) {   //inner loop for numCollumns
                System.out.print(numCollumns + " ");    //outputs pattern
            }     //closes inner loop
            System.out.println();     //new line
        }   //ends outer loop
  
  }   //ends the main method
}     //ends the class