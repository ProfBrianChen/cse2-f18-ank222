//Andrew Kelly
//CSE02
//9/20/18

/* This program will serve multiple tasks:
1st, ask the user if they would like random numbers or if they would like to input their own
2nd, be able to generate 2 random numbers, or accept 2 given numbers
3rd, determine and output the slang terminology via the game Craps.
This program will be coded using if else statements. */

import java.util.Scanner;     //This imports the scanner that will be needed if the user wants to input their own values
import java.lang.Math;        //This imports the math class that will be used to generate random numbers
public class CrapsIf {    //This begins the class and names it "CrapsIf"
  
  public static void main(String[] args) {    //This introduces the main method, it's needed for every java program
    
    Scanner firstScanner = new Scanner(System.in);   //This declares the scanner
    Scanner secondScanner = new Scanner(System.in);   //This declares a second scanner
    
      
    int rollOne = 0;    //Assigns a non 1-6 value for manual roll one, declaring it to be an integer
    int rollTwo = 0;    //Assigns a non 1-6 value for manual roll two, declaring it to be an integer
    int randomRollOne = 0;   //Assigns an integer variable to random roll one
    int randomRollTwo = 0;    //Assigns an integer variable to random roll two
      
    //Below is some inital introduction
    System.out.println("Welcome to the Craps Simulator!");    //Outputs a greeting
    System.out.println("You will be given the chance to input your own dice values, or you can have them randomly generated");    //Outputs options
    System.out.print("Type: '1' to input values or '2' for randomly generated values: ");   //Outputs instructions for the user
    
    int userChoice = firstScanner.nextInt();   //Allows for user input, will correlate with line 29
  
    if (userChoice == 1) {    //Starts the else if statement for user input variable
      System.out.print("Enter your first die roll value, an integer from 1-6: ");   //Outputs whats in ""
        rollOne = secondScanner.nextInt();    //Accepts user input of roll one for line 34
      System.out.print("Enter your second die roll value, an integer from 1-6: ");  //Outputs whats in ""
       rollTwo = secondScanner.nextInt();     //Accepts user input for line 36
      
          //Below is if else statements for manual rolls, lines 41-110 deal with manual input
          //Below is output for inputted rolls, these are highly repetitive, comments for lines 41-43 will apply to all with only numerical differences    
      if ((rollOne == 1) && (rollTwo == 1)) {                       //starts if statement for roll 1 & 1
        System.out.println("This is reffered to as: Snake Eyes");   //Outputs snake eyes for roll 1 & 1
      }                                                             //Ends if statement for rolls 1 & 1
      else if (((rollOne == 1) && (rollTwo == 2)) || ((rollOne == 2) && (rollTwo == 1))) {
        System.out.println("This is reffered to as: Ace Deuce");
      }
      else if (((rollOne == 1) && (rollTwo == 3)) || ((rollOne == 3) && (rollTwo == 1))) {
        System.out.println("This is reffered to as: Easy Four");
      }
      else if (((rollOne == 1) && (rollTwo == 4)) || ((rollOne == 4) && (rollTwo == 1))) {
        System.out.println("This is reffered to as: Fever Five");
      }
      else if (((rollOne == 1) && (rollTwo == 5)) || ((rollOne == 5) && (rollTwo == 1))) {
        System.out.println("This is reffered to as: Easy Six");
      }
      else if (((rollOne == 1) && (rollTwo == 6)) || ((rollOne == 6) && (rollTwo == 1))) {
        System.out.println("This is reffered to as: Seven Out");
      }
       else if ((rollOne == 2) && (rollTwo == 2)) {
        System.out.println("This is reffered to as: Hard Four");
      }
        else if (((rollOne == 2) && (rollTwo == 3)) || ((rollOne == 3) && (rollTwo == 2))) {
        System.out.println("This is reffered to as: Fever Five");
      }
       else if (((rollOne == 2) && (rollTwo == 4)) || ((rollOne == 4) && (rollTwo == 2))) {
        System.out.println("This is reffered to as: Easy Six");
      }
       else if (((rollOne == 2) && (rollTwo == 5)) || ((rollOne == 5) && (rollTwo == 2))) {
        System.out.println("This is reffered to as: Seven Out");
      } 
       else if (((rollOne == 2) && (rollTwo == 6)) || ((rollOne == 6) && (rollTwo == 2))) {
        System.out.println("This is reffered to as: Easy Eight");
      }
       else if ((rollOne == 3) && (rollTwo == 3)) {
        System.out.println("This is reffered to as: Hard Six");
      }
       else if (((rollOne == 3) && (rollTwo == 4)) || ((rollOne == 4) && (rollTwo == 3))) {
        System.out.println("This is reffered to as: Seven Out");
      } 
       else if (((rollOne == 3) && (rollTwo == 5)) || ((rollOne == 5) && (rollTwo == 3))) {
        System.out.println("This is reffered to as: Easy Eight");
      }
       else if (((rollOne == 3) && (rollTwo == 6)) || ((rollOne == 6) && (rollTwo == 3))) {
        System.out.println("This is reffered to as: Nine");
      } 
       else if ((rollOne == 4) && (rollTwo == 4))  {
        System.out.println("This is reffered to as: Hard Eight");
      }  
       else if (((rollOne == 4) && (rollTwo == 5)) || ((rollOne == 5) && (rollTwo == 4))) {
        System.out.println("This is reffered to as: Nine");
      }
       else if (((rollOne == 4) && (rollTwo == 6)) || ((rollOne == 6) && (rollTwo == 4))) {
        System.out.println("This is reffered to as: Easy Ten");
      }         
       else if ((rollOne == 5) && (rollTwo == 5))  {
        System.out.println("This is reffered to as: Hard Ten");
      }         
       else if (((rollOne == 5) && (rollTwo == 6)) || ((rollOne == 6) && (rollTwo == 5))) {
        System.out.println("This is reffered to as: Yo-leven");
      }
       else if ((rollOne == 6) && (rollTwo == 6))  {
        System.out.println("This is reffered to as: Boxcars");
      }  
      else if ((rollOne <= 0) || (rollTwo <= 0)) {    //Takes into account the user inputting a faulty variable below or equal to 0
        System.out.println("You have entered an invalid number");   //Tells the user they entered an invalid number
      } 
      else if ((rollOne > 6) || (rollTwo > 6)) {    //Takes into account the user inputting a faulty variable above 6
        System.out.println("You have entered an invalid number");   //Tells the user they entered an invalid number
      } 
    }   //Closes the if statement for user inpput
    if (userChoice == 2) {     //Starts the if statement for random variable
      randomRollOne = (int)(Math.random()*6)+1;   //Assigns an integer variable to random roll one
      randomRollTwo = (int)(Math.random()*6)+1; 
      System.out.println("Your random roll produced a: " + randomRollOne + ", and a: " + randomRollTwo);
    }   //Closes the if statement for random roll
    
    
    //Below are my if else statements for random rolls, they are highly repetitive, comments for lines 120-122 apply to all of them
    //Lines 120-173 deal with randomly generated rolls
    if ((randomRollOne == 1) && (randomRollTwo == 1)) {           //Takes rolls 1 & 1 into account
      System.out.println("This is reffered to as: Snake Eyes");   //Outputs Snake Eyes due to the 1 & 1 roll
    }                                                             //ends the if statement for 1 & 1
    else if (((randomRollOne == 1) && (randomRollTwo == 2)) || ((randomRollOne == 2) && (randomRollTwo == 1))) {
      System.out.println("This is reffered to as: Ace Deuce");
    }
    else if (((randomRollOne == 1) && (randomRollTwo == 3)) || ((randomRollOne == 3) && (randomRollTwo == 1))) {
      System.out.println("This is reffered to as: Easy Four");
    }
    else if (((randomRollOne == 1) && (randomRollTwo == 4)) || ((randomRollOne == 4) && (randomRollTwo == 1))) {
      System.out.println("This is reffered to as: Fever Five");
    }
    else if (((randomRollOne == 1) && (randomRollTwo == 5)) || ((randomRollOne == 5) && (randomRollTwo == 1))) {
      System.out.println("This is reffered to as: Easy Six");
    }
    else if (((randomRollOne == 1) && (randomRollTwo == 6)) || ((randomRollOne == 6) && (randomRollTwo == 1))) {
      System.out.println("This is reffered to as: Seven Out");
    }
    else if ((randomRollOne == 2) && (randomRollTwo == 2))  {
      System.out.println("This is reffered to as: Hard Four");
    }
    else if (((randomRollOne == 2) && (randomRollTwo == 3)) || ((randomRollOne == 3) && (randomRollTwo == 2))) {
      System.out.println("This is reffered to as:Fever Five");
    }
    else if (((randomRollOne == 2) && (randomRollTwo == 4)) || ((randomRollOne == 4) && (randomRollTwo == 2))) {
      System.out.println("This is reffered to as: Easy Six");
    }
    else if (((randomRollOne == 2) && (randomRollTwo == 5)) || ((randomRollOne == 5) && (randomRollTwo == 2))) {
      System.out.println("This is reffered to as: Seven Out");
    }
    else if (((randomRollOne == 2) && (randomRollTwo == 6)) || ((randomRollOne == 6) && (randomRollTwo == 2))) {
      System.out.println("This is reffered to as: Easy Eight");
    }
    else if ((randomRollOne == 3) && (randomRollTwo == 3)) {
      System.out.println("This is reffered to as: Hard Six");
    }
    else if (((randomRollOne == 3) && (randomRollTwo == 4)) || ((randomRollOne == 4) && (randomRollTwo == 3))) {
      System.out.println("This is reffered to as: Seven Out");
    }
    else if (((randomRollOne == 3) && (randomRollTwo == 5)) || ((randomRollOne == 5) && (randomRollTwo == 3))) {
      System.out.println("This is reffered to as: Easy Eight");
    }
    else if (((randomRollOne == 3) && (randomRollTwo == 6)) || ((randomRollOne == 6) && (randomRollTwo == 3))) {
      System.out.println("This is reffered to as: Nine");
    }
    else if ((randomRollOne == 4) && (randomRollTwo == 4)) {
      System.out.println("This is reffered to as: Hard Eight");
    }
    else if (((randomRollOne == 4) && (randomRollTwo == 5)) || ((randomRollOne == 5) && (randomRollTwo == 4))) {
      System.out.println("This is reffered to as: Nine");
    }
    else if (((randomRollOne == 4) && (randomRollTwo == 6)) || ((randomRollOne == 6) && (randomRollTwo == 4))) {
      System.out.println("This is reffered to as:Easy Ten");
    }
    else if ((randomRollOne == 5) && (randomRollTwo == 5)) {
      System.out.println("This is reffered to as: Hard Ten");
    }
    else if (((randomRollOne == 5) && (randomRollTwo == 6)) || ((randomRollOne == 6) && (randomRollTwo == 5))) {
      System.out.println("This is reffered to as: Yo-leven");
    }
    else if ((randomRollOne == 6) && (randomRollTwo == 6))  {
      System.out.println("This is reffered to as: Boxcars");
    } 
    
   
    System.out.println("Thanks for playing!");    //Thanks the user for playing and is last line of output
    
    
  }   //This ends the main method
}   //This ends the class and the code