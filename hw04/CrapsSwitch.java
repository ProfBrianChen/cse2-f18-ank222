//Andrew Kelly
//CSE02
//9/20/18

/* This program will serve multiple tasks:
1st, ask the user if they would like random numbers or if they would like to input their own
2nd, be able to generate 2 random numbers, or accept 2 given numbers
3rd, determine and output the slang terminology via the game Craps.
This code will be run using switch statements*/

import java.util.Scanner;   //This imports the scanner that the user can use to input dice value
import java.lang.Math;      //Imports the math class that will be used during randomization
public class CrapsSwitch {    //Introduces the class "CrapsSwtich"
  
  public static void main(String[] args) {    //Introduces the main method needed for every java program
    
    Scanner myScanner = new Scanner(System.in);   //This declares the scanner
    
    int randomRollOne = (int)(Math.random()*6)+1;    //defines the first random roll to be an integer from 1-6
    int randomRollTwo = (int)(Math.random()*6)+1;   //defines the second random roll to be an integer from 1-6
    int manualRollOne = 0;      //Declares the first manual roll to be a non 1-6 value
    int manualRollTwo = 0;      //Declares the second manual roll to be a non 1-6 value
    
   //Below is some general introduction
    System.out.println("Welcome to the Craps Simulator!");    //Outputs a greeting
    System.out.println("You will be given the chance to input your own dice values, or you can have them randomly generated");    //Outputs options
    System.out.print("Type: '1' for input and '2' for random: ");   //Outputs instructions for the user
    int userChoice = myScanner.nextInt();   //Allows for user input, will correlate with line 27
    
    //Below is switch statements for user choice of manual vs random
    switch (userChoice) {   //Starts the switch that deals with random vs chosen
      case 1:   //This is activated if the user chooses 1, they want to input their own values
        System.out.println("Please enter an integer value from 1-6 for your first die roll: ");   //Prompts user to enter value
        manualRollOne = myScanner.nextInt();    //Accept user value
        System.out.println("Please enter and integer value from 1-6 for your second die roll");   //Prompts user to enter value
        manualRollTwo = myScanner.nextInt();    //Accepts user value
        //Below starts switch statements for user inputted values, it is highly repetitive and every line need not be coded bc they are all the same with numerical variations
        switch(manualRollOne) {      //Start of switch statements for when the first roll is 1 and the second roll is 1-6
         case 1:    //declares first roll to be 1, lines 39-62 deal with first roll being 1
           switch(manualRollTwo){   //switch statement for second roll after first roll is one
             case 1:      //roll two is a 1
               System.out.println("You rolled a: Snake Eyes");
               break;     //ends case 1 roll two
             case 2:      //roll two is a 2
               System.out.println("You rolled an: Ace Deuce");
               break;     //ends case 2 roll two
             case 3:      //roll two is a 3
               System.out.println("You rolled an: Easy Four");
               break;     //ends case 3 roll two
             case 4:      //roll two is a 4
               System.out.println("You rolled a: Fever Five");
               break;     //ends case 4 roll two
             case 5:      //roll two is a 5
               System.out.println("You rolled an: Easy Six");
               break;     //ends case 5 roll 2
             case 6:      //roll two is a 6
               System.out.println("You rolled a: Seven Out");
               break;     //ends case 6 roll two
             default:   //declares a default statement incase user inputs a non 1-6 integer
            System.out.println("You have entered an invalid Number");   //Outputs the invalid statement
           }    //Closes the roll two for values with 1 as first roll
        break;  //ends roll one being 1
         case 2:    //For when first roll is 2 
           switch(manualRollTwo){   //Lines 64-86 deal with roll one being a 2 and the other options
             case 1: 
               System.out.println("You rolled an:Ace Deuce");
               break;
             case 2: 
               System.out.println("You rolled a: Hard Four");
               break;
             case 3:
               System.out.println("You rolled a: Fever Five");
               break;
             case 4:
               System.out.println("You rolled an: Easy Six");
               break;
             case 5:
               System.out.println("You rolled a: Seven Out");
               break;
             case 6:  
               System.out.println("You rolled an: Easy Eight");
               break;
             default:
            System.out.println("You have entered an invalid Number");
           }   //Closes roll two after roll one is 2
         break;   //ends roll one being 2
          case 3:  //For when first roll is 3, lines 87-110 deal with first role being 3
           switch(manualRollTwo){
             case 1:
               System.out.println("You rolled an: Easy Four");
               break;
             case 2: 
               System.out.println("You rolled a: Fever Five");
               break;
             case 3:
               System.out.println("You rolled a: Hard Six");
               break;
             case 4:
               System.out.println("You rolled a: Seven Out");
               break;
             case 5:
               System.out.println("You rolled an: Easy Eight");
               break;
             case 6:  
               System.out.println("You rolled a: Nine");
               break;
               default:
            System.out.println("You have entered an invalid Number");
           }      //closes roll two after roll one is 3
         break;   //ends roll one being 3
          case 4:   //First roll is 4, lines 111-133 deal with roll one being 4
           switch(manualRollTwo){
             case 1:
               System.out.println("You rolled a: Fever Five");
               break;
             case 2: 
               System.out.println("You rolled an: Easy Six");
               break;
             case 3:
               System.out.println("You rolled a: Seven Out");
               break;
             case 4:
               System.out.println("You rolled a: Hard Eight");
               break;
             case 5:
               System.out.println("You rolled a: Nine");
               break;
             case 6:  
               System.out.println("You rolled an: Easy Ten");
               break;
               default:
            System.out.println("You have entered an invalid Number");
           }        //closes roll two when roll one is 4
         break;     //ends roll one being 4
          case 5:   //first roll is 5, lines 135-158 deal with roll one being 5
           switch(manualRollTwo){
             case 1:
               System.out.println("You rolled an: Easy Six");
               break;
               case 2: 
               System.out.println("You rolled a: Seven Out");
               break;
             case 3:
               System.out.println("You rolled an: Easy Eight");
               break;
             case 4:
               System.out.println("You rolled a: Nine");
               break;
             case 5:
               System.out.println("You rolled a: Hard Ten");
               break;
             case 6:  
               System.out.println("You rolled a: Yo-Leven");
               break;
               default:
            System.out.println("You have entered an invalid Number");
           }          //closes roll two after roll one is 5
         break;       //ends roll one being 5
          case 6:     //first roll is 6, lines 159-182 deal with roll one being 6
           switch(manualRollTwo){
             case 1:
               System.out.println("You rolled a: Seven Out");
               break;
               case 2: 
               System.out.println("You rolled an: Easy Eight");
               break;
             case 3:
               System.out.println("You rolled a: Nine");
               break;
             case 4:
               System.out.println("You rolled an: Easy Ten");
               break;
             case 5:
               System.out.println("You rolled a: Yo-Leven");
               break;
             case 6:  
               System.out.println("You rolled a: Boxcars");
               break;
                default:
                  System.out.println("You have entered an invalid number");
           }          //closes roll two after roll one is 6 
         break;       //ends roll one being 6 
          default:    //gives a default statement incase values entered are not 1-6
            System.out.println("You have entered an invalid number");
        }         //ends the manual roll switch statement that was started on line 38
         break;    //ends the manual roll switch statements
        
        //Below are switches if the user wants randomly generated values
      case 2:   //This is activated if the user chooses 2, they want random rolls
        System.out.println("Your random roll produced a: " + randomRollOne + ", and a: " + randomRollTwo);  //Tells the user what their rolls were
        
        //Below the switch statements follow the same exact pattern as for manual coding, see specific comments above
        switch(randomRollOne) {      //Start of switch statements for when the first roll is 1 and the second roll is 1-6
         case 1:    //declares first roll to be 1
           switch(randomRollTwo){   //switch statement for second roll after first roll is one
             case 1:      //roll two is a 1
               System.out.println("You rolled a: Snake Eyes");
               break;
             case 2:      //roll two is a 2
               System.out.println("You rolled an: Ace Deuce");
               break;
             case 3:      //roll two is a 3
               System.out.println("You rolled an: Easy Four");
               break;
             case 4:      //roll two is a 4
               System.out.println("You rolled a: Fever Five");
               break;
             case 5:      //roll two is a 5
               System.out.println("You rolled an: Easy Six");
               break;
             case 6:      //roll two is a 6
               System.out.println("You rolled a: Seven Out");
               break;
           }
        break;  //ends roll one being 1
            
       case 2:    //For when first roll is 2 
           switch(randomRollTwo){
             case 1:
               System.out.println("You rolled an:Ace Deuce");
               break;
             case 2:
               System.out.println("You rolled a: Hard Four");
               break;
             case 3:
               System.out.println("You rolled a: Fever Five");
               break;
             case 4:
               System.out.println("You rolled an: Easy Six");
               break;
             case 5:
               System.out.println("You rolled a: Seven Out");
               break;
             case 6: 
               System.out.println("You rolled an: Easy Eight");
               break;
           }  
         break;   //ends role one being 2
          case 3:  //For when first roll is 3 
           switch(randomRollTwo){
             case 1:
               System.out.println("You rolled an: Easy Four");
               break;
             case 2:
               System.out.println("You rolled a: Fever Five");
               break;
             case 3:
               System.out.println("You rolled a: Hard Six");
               break;
             case 4:
               System.out.println("You rolled a: Seven Out");
               break;
             case 5:
               System.out.println("You rolled an: Easy Eight");
               break;
             case 6: 
               System.out.println("You rolled a: Nine");
               break;
           }
         break;   //ends roll one being 3
          case 4:   //For when roll one is 4
           switch(randomRollTwo){
             case 1:
               System.out.println("You rolled a: Fever Five");
               break;
             case 2:
               System.out.println("You rolled an: Easy Six");
               break;
             case 3:
               System.out.println("You rolled a: Seven Out");
               break;
             case 4:
               System.out.println("You rolled a: Hard Eight");
               break;
             case 5:
               System.out.println("You rolled a: Nine");
               break;
             case 6: 
               System.out.println("You rolled an: Easy Ten");
               break;
           }
         break;       //ends roll one being 4
          case 5:     //For when roll one is 5
           switch(randomRollTwo){
             case 1:
               System.out.println("You rolled an: Easy Six");
               break;
               case 2:
               System.out.println("You rolled a: Seven Out");
               break;
             case 3:
               System.out.println("You rolled an Easy Eight");
               break;
             case 4:
               System.out.println("You rolled a: Nine");
               break;
             case 5:
               System.out.println("You rolled a: Hard Ten");
               break;
             case 6: 
               System.out.println("You rolled a: Yo-Leven");
               break;
           }
         break;      //ends roll one being 5
          case 6:    //for when roll one is 6
           switch(randomRollTwo){
             case 1:
               System.out.println("You rolled a: Seven Out");
               break;
               case 2:
               System.out.println("You rolled an: Easy Eight");
               break;
             case 3:
               System.out.println("You rolled a: Nine");
               break;
             case 4:
               System.out.println("You rolled an: Easy Ten");
               break;
             case 5:
               System.out.println("You rolled a: Yo-Leven");
               break;
             case 6: 
               System.out.println("You rolled a: Boxcars");
               break;
           }
         break;     //ends roll one being 6
       }  
       break;       //closes overall switch
        
     default:    //This is in case the user enters something other than 1 or 2 at first stage of input, manual vs random
        System.out.println("You have entered an invalid number");     //outputs error statement
        break;    //closes the default
    }   //Closes the userChoice switch
   
  
  }   //This ends the main method
}     //This ends the class