//Andrew Kelly
//CSE02
//11.16.18

//This lab will ilustrate the effects of passing array as method arguments

import java.util.*;   //imports java.utils
public class lab09{   //starts class
  public static void main (String[] args){    //starts main
    int [] array0 = {1,2,3,4,5,6,7,8,9,};   //declares literal int array array 0
    int [] array1 = copy(array0);       //calls array0 through copy method and sets to array1
    int [] array2 = copy(array0);       //calls array0 through copy method and sets to array2
    inverter(array0);   //sends array0 through inverter
    print(array0);      //sends array0 through print...prints
    System.out.println("");     //new line
    inverter2(array1);    //sends array1 through inverter2
    print(array1);        //sends array1 through print...prints
    System.out.println("");     //new line
    int [] array3 = inverter2(array2);    //declares array 3 and sets it to the iverter2 of array2
    print(array3);      //print method for array3
    System.out.println("");   //new line
  }   //ends main
  
  public static int [] copy(int[] array0){     //starts copy method
    int [] copyArray = new int [9];   //declare and allocates copyArray same length as array0 in main
    for(int i=0; i < copyArray.length; i++){     //starts for loop to run through array0
      copyArray[i] = array0[i];      
    }   //closes for to copy array0 into copyArray
  return copyArray;
  }     //closes copy method
  
  public static void inverter(int[] array){      //starts inverter method
    for(int i = 0; i < array.length / 2; i++){       //sets for loop to go through half the array
      int tempValue = array[i];    //sets tempValue to array0
      array[i] = array[array.length - i - 1];   //reverses order
      array[array.length - i - 1] = tempValue;
    }   //closes for
    return ;    //void type so no rturn
  }   //closes inverter method
  
  public static int[] inverter2(int[] array){     //starts inverter2 method
    int [] copiedArray = copy(array);   //creates copiedarray and sets it to array through copy method
    inverter(copiedArray);    //sends copiedArray through inverter method
    return copiedArray;   //returns copiedArray
  }   //closes inverter2 method
  
  public static void print(int[] array){     //starts print methid
    for(int i=0; i < array.length; i++){      //for loop to go through array
      System.out.print(array[i]);     //prints the array
    }   //closes for loop
    return;   //void type so no return
  }   //closes print method
   
}   //ends class