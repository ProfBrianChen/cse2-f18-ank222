//Andrew Kelly
//CSE02
//11.22.18

/*This program will deal with an int array of index15
The program will allow for user input, while checking if input is acceptable
The program will run a binary search on the sorted array, and return the key if possible
The program will scramble the array and then run a linear search, doing the same thing*/

import java.util.*;   //imports utils

public class CSE2Linear {    //starts class
  public static void main (String [] args){   //starts main
    boolean stopLoop = false;   //decalre and initialize stopLoop boolean to false
    Scanner myScanner = new Scanner(System.in);   //creates scanner
    int[] userArray = new int[15];    //declare and initalize array of size 15 
    System.out.println("Enter 15 ascending ints for final grades in CSE2:");    //output
   for (int i = 0; i < userArray.length; i++) {     //for loop to go through array
     int value = 0;  //declare and initalize value
   if (myScanner.hasNextInt()){   //if the scanner goes int
      value = myScanner.nextInt();     //allows value to be inputted
    if (value <= -1){   //if for values below 0
       System.out.println("You have entered a value below 0, please enter a value between 0 and 100");    //error statement
     i--;         //sets i back to allow for correct input
     }    //closes if
     else if (value >= 101){    //else if for value over 100
       System.out.println("You have entered a value above 100, please enter a value between 0 and 100");    //error statement
             i--;     //sets i back to allow for correct input
     }    //closes else if
    else if ( i == 0){    //else if for i = 0
       userArray[i] = value;    //sets value into the array
    }   //closes else if
    else {    //starts else
      if (userArray[i-1] <= value){   //if statement to cehck ascending order
        userArray[i] = value;   //if it is ascending, sets value to array
      }   //closes if
      else {    //when its not in ascending
        System.out.print("You have entered a value that is not greater than or equal to your prior value, ");   //error statement
        System.out.println("please enter a new value that is in ascending order.");   //error statement
        i--;    //sets i back to allow for correct input
      }   //closes else for ascending
    }   //closes else after range
   }    //closes int for 
     else{    //else for when not an integer
       System.out.println("You have entered a non-integer value, please enter an integer.");    //error statement
       i--;
         break;    //continues
        // value =  myScanner.nextInt();   //allows for reinput
     }    //closes else
   }   //closes for loop
    print(userArray);   //calls print method to print the user array
    System.out.println();   //new line
    System.out.print("Enter a grade to search for: ");      //output
    int key = myScanner.nextInt();    //accepts user input for the key
    System.out.println(binarySearch(userArray, key));   //calls binary search method with the array and keuy, prints
    System.out.println("Scrambled:");     //outputs ""
    print(scramble(userArray));   //calls scramble method for array within print method, prints scrambled array
    System.out.println();   //new line
    System.out.println("Enter a grade to search for: ");    //outputs ""
    int key2 = myScanner.nextInt();   //accepts input for key2
    System.out.println(linearSearch(userArray, key2));    //outputs linearSearch method for array and key2
  }   //ends main
//Lines ~25-41 that deal with the binary search method were utilzied from Prof. Carr with permisson
  public static String binarySearch(int[] userArray, int key) {     //starts binarySearch method
  int iterations = 1;   //sets iteration count to 1, this way, if the key is found on the first step, it's counted as an iteration
  int low = 0;    //sets low
  int high = userArray.length-1;    //sets high
  while(high >= low) {    //starts while loop for when high >=low
   int mid = (low + high)/2;   //sets mid to be the mid
   if (key < userArray[mid]) {   //starts if statement for when key < midpoint
    high = mid - 1;             //resets the high to the mid - 1
    iterations++;       //increases iterations when key not found
   }     //closes if statement
   else if (key == userArray[mid]) {   //starts else if for when the key is at the mid
    return (key + " was found in the list with " + iterations + " iterations.");     //returns the mid/key
   }   //closes else if
   else {    //starts else, when the mid is less than the key
    low = mid + 1;      //sets the low to mid + 1
     iterations++;      //increases iterations when key not found
   }   //closes else
  }   //closes while
  return (key + " was not found in the list with " + iterations + " iterations.");    //dummy statement for if key is not found, 
 }   //closes binarySearch method

  //Lines ~44-51 that deal with the linearSearch method were utilzied from Prof. Carr with permission
  public static String linearSearch(int[] userArray, int key2) {     //starts linearSearch method
     int iterations = 1;        //sets iteration count to 1, this way, if the key is found on the first step, it's counted as an iteration  
  for (int i = 0; i < userArray.length; i++) {    //for statemnt to go through array
    if (key2 != userArray[i]){    //if statement for when key2 is not found
      iterations++;     //increases iterations when key not found
    }
    else if (key2 == userArray[i]){   //when the key is found
        return (key2 + " was found in the list with " + iterations + " iterations.");      //returns the key
    }   //closes if
  }         //closes for
 return (key2 + " was not found in the list with " + iterations + " iterations.");    //returns dummy if key is not found
 }   //closes linear method

public static int[] scramble(int[] userArray){    //starts scramble method with array argument
	Random random = new Random();  // Random number generator
	for (int i=0; i<userArray.length; i++) {      //for loop to go through array
	    int randomPlace = random.nextInt(userArray.length);   //variable for random place, sets to random spot of array
	    int tempVal = userArray[i];    //tempVal for the array holder
	    userArray[i] = userArray[randomPlace];    //sets new spot to the randomPlace
	    userArray[randomPlace] = tempVal;   //brings the old tempVal to the new place
	}   //ends for loop 
	return userArray;   //returns the now scrambled array
	}   //closes scramble method
  
public static void print(int[] userArray){     //starts print methid
    for(int i=0; i < userArray.length; i++){      //for loop to go through array
      System.out.print(userArray[i] + " ");     //prints the array
    }   //closes for loop
    return;   //void type so no return
  }   //closes print method
  
}   //ends class