//Andrew Kelly
//CSE02
//11.23.18

/*This program will utilzie a given main method to
take an array, search for a value, and remove said value to create a new array*/

import java.util.Scanner;   //imports scanner
public class RemoveElements{    //starts class
  public static void main(String [] arg){   //main
    Scanner scan=new Scanner(System.in);    //creates scanner
int num[]=new int[10];    //creates array num of size 10
int newArray1[];    //creates newArray1
int newArray2[];    //creates newArray2
int index,target;   //creates ints index and target
    String answer="";   //creates string variable
    do{   //starts do while
      System.out.print("Random input 10 ints [0-9]");   //outputs ""
      num = randomInput();    //takes randomInput
      String out = "The original array is:";    //outputs
      out += listArray(num);    //sets the string variable out to the listArray method with num
      System.out.println(out);    //outputs out variable
 
      System.out.print("Enter the index ");   //outputs ""
      index = scan.nextInt();   //takes user input for index
      newArray1 = delete(num,index);    //sets newArray1 to the delete method
      String out1="The output array is ";   //sets the string variable out1 to ""
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);   //outputs the new variable out1
 
      System.out.print("Enter the target value ");    //outputs ""
      target = scan.nextInt();    //accepts user input for target
      newArray2 = remove(num, target);    //sets newArray2 thrugh remove method
      String out2="The output array is ";   //string variable out2 with ""
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);   //outputs new out2 variable
       
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");   //outputs ""
      answer=scan.next();   //accepts user input for char
    }while(answer.equals("Y") || answer.equals("y"));   //while loop for y or Y
  }   //closes the main method
 
  public static String listArray(int num[]){    //method listArray, returns a string
    String out="{";   //sets string variable out to "{"
    for(int j=0;j<num.length;j++){    //for loop to go through array
      if(j>0){    //if statement for when j bigger than 0
        out+=", ";    //sets out to the out with a comma
      }   //closes if
      out+=num[j];    //sets out to out + new array value
    }   //closes for loop
    out+="} ";    //closes out variable with bracket
    return out;   //returns out variable
  }   //closes listArray method
  
  public static int [] randomInput(){   //starts randomInput method
    int [] randomArray = new int[10];   //create and declare randomArray size 10
    for (int i = 0; i < randomArray.length; i++){   //for loop to go through randomArray
      randomArray[i] = (int)(Math.random()*10);   //sets each value to a random int between 0 - 9
    }   //closes for loop
    return randomArray;   //returns the new randomArray
  }   //closes randomInput method
  
  public static int[] delete(int list [], int pos){   //delete method to return an int array
    int [] shorterArray = new int[9];   //creates a new shorterArray with length 9 instead of 10
    while(pos < 0 || pos > list.length) {    //if statement for when pos is <0 or > length
        System.out.println("You entered an out of range value.");   //outputs ""
      break;
    }   //closes if
    int j = 0;    //new variable j
    for(int i = 0; i < list.length; i++) {    //for loop to go through array
        if(i == pos) {    //if for when i == pos
            i++;    //increases i
        }   //closes if
        shorterArray[j++] = list[i];    //sets shorterArray to list
    }   //closes for
    return shorterArray;    //returns shorterArray
}   //closes delete method
  
 public static int[] remove (int[] list, int target){    //remove mthod
        int counter = 0;    //sets counter to 0
        for(int i = 0; i<list.length; i++){   //for loop to go through list        
            if(list[i] == target){   //if value is the target
            counter++;    //increment count
            }   //closes if
        }       //closes for     
        int[] newArray = new int[list.length-counter];    //creates newArray of length list - count, count was increased based on how many target hits
      int position = 0;     //start position and set to 0
        for(int i = 0; i<list.length; i++){     //for loop to go through list 
          
          if(list[i] == target){   //if list value = target     
               //do nothing    
            }   //closes if
            else{     //else, target is not found
                newArray[position] = list[i];      //newArray is same is old list, copies
                position++;   //increment  position
            }   //closes else
        }   //closes for
        return newArray;    //returns newArray, if target was found, will be altered, if not, wont be altered
 }    //closes remove method
  
}     //closes the class
