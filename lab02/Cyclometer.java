//Andy Kelly - CSE 002 - 9/7/18
//This program will...
//print the number of minutes for each trip,
//print the number of counts for each trip,
//print the distance of each trip in miles,
//print the distance for the two trips combined,
//all according to my bicycle cyclometer.

public class Cyclometer { // start of class
    
    public static void main (String [] args) { //main method required for every java program
      
      int secsTrip1=480;  // stores number of seconds in trip 1
      int secsTrip2=3220;  // stores number of seconds in trip 2
      int countsTrip1=1561;  // stores number of counts in trip 1
      int countsTrip2=9037; // stores number of counts in trip 2
      
      double wheelDiameter=27.0,  // creating and defining wheel diamter, using a double to allot for decimals
      PI=3.14159, // defining variable pi
      feetPerMile=5280,  // defining the number of feet per mile to be 5280
      inchesPerFoot=12,   // defining the number of inches per foot to be 12
      secondsPerMinute=60;  // defining the number of seconds per minute to be 60
      double distanceTrip1, distanceTrip2,totalDistance;  // defining variables for the distance of the trips
      System.out.println("Trip 1 took "+
                (secsTrip1/secondsPerMinute)+" minutes and had "+
                 countsTrip1+" counts."); // outputs how long trip 1 was
      System.out.println("Trip 2 took "+
                (secsTrip2/secondsPerMinute)+" minutes and had "+
                 countsTrip2+" counts."); // outputs how long trip 2 was

        // below are calculations for the distance of the trips and eventually the total distance
      distanceTrip1=countsTrip1*wheelDiameter*PI;
        // Above gives distance in inches
        //(for each count, a rotation of the wheel travels
        //the diameter in inches times PI)
      distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
      distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
      totalDistance=distanceTrip1+distanceTrip2;
      
       //Print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles"); //outputs "Trip 1 was *blank* miles"
      System.out.println("Trip 2 was "+distanceTrip2+" miles"); //outputs "Trip 2 was *blank* miles"
      System.out.println("The total distance was "+totalDistance+" miles"); //ouputs "The total distance was *blank* miles"
  


    } // end of main method
} // end of class