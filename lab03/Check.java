//Andrew Kelly
//9/14/18
//CSE 02-lab03

/*This lab will use a scanner class in order to
obtain from the user the original cost of the check,
the percentage tip they wish to pay,
and the number of ways the check will be split.*/

import java.util.Scanner;   //This imports the scanner


public class Check {   //This is the introduction of the class
  
  public static void main(String[] args) {  //This is the main method, it is required for every java program
    
    Scanner myScanner = new Scanner( System.in );   //This declares the instance of the scanner 
    
    //Below starts initial output that will act as user input/data collection
    System.out.print("Enter the original cost of the check in the form xx.xx: ");   //This outputs exactly what is in the ""
    double checkCost = myScanner.nextDouble();    //This accepts user input for line 20. Since line 20 has no ln after print means the next line will not be a new line
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );    //This outputs what is in the ""
    double tipPercent = myScanner.nextDouble();   //This accepts user input for line 23
    tipPercent /= 100;    //This converts the percent into a decimal
    
    System.out.print("Enter the number of people who went out to dinner: ");    //Outputs what is in the ""
    int numPeople = myScanner.nextInt();    //This accepts user input for line 27
    
    //Below starts to declare some new variables needed
    double totalCost;   //Allows total cost to include decimals
    double costPerPerson;      //Allows cost per person to include decimals
    int dollars, dimes, pennies;    //Declares dollars, dimes and pennies to be whole numbers
    
    totalCost = checkCost * (1 + tipPercent);   //Calculates total cost
    costPerPerson = totalCost / numPeople;      //Calculates cost per person
    dollars = (int) costPerPerson;     //Forces the dollars to be an integer rather than the double that cost per person wants to be
    dimes = (int) (costPerPerson * 10) % 10;   //Forces dimes to be an integer, does some calculations to determine dime amount
    pennies = (int) (costPerPerson * 100) % 10;    //Forces pennies to be an integer, does some calculations
    
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);   //This is the actual output of how much each person owes, in dollars, dimes and pennies
    
    
  }    //Matches up with the main method

}     //Matches up with the class
