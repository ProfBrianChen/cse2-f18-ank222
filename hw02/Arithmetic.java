//Andrew Kelly
//9/9/18
//CSE 02
//Above is my intro of comments, it states my name, the date and the class

 public class Arithmetic{ // starts code
 
  public static void main(String args[]){ //starts string
    
    //This is me inputting variables that will be needed later
    int numPants = 3;           //Number of pairs of pants
    double pantsPrice = 34.98;  //Cost per pair of pants
    int numShirts = 2;          //Number of sweatshirts
    double shirtPrice = 24.99;  //Cost per shirt
    int numBelts = 1;           //Number of belts
    double beltPrice = 33.99;    //cost per belt
    double paSalesTax = 0.06;   //the tax rate
    
    //This is me declaring some variables as doubles
    double totalCostOfPants;   //total cost of pants
    double totalCostOfShirts;  //total cost of shirts
    double totalCostOfBelts;   //total cost of belts
    double salesTaxOnPants;    //The pa sales tax applied to pants
    double salesTaxOnShirts;   //The pa sales tax applied to shirts
    double salesTaxOnBelts;    //The pa sales tax applied to belts
    double costOfPurchNoTax;   //The total cost of the purchase, before tax is applied
    double totalAmountTaxed;   //The total amount taxed on the purchase
    double finalAmountPayed;   //The final amount you are charged at the end of your transaction, tax included
    
    //This is me assigning values to the variables I just declared
    totalCostOfPants = (numPants * pantsPrice);   //This is multiplying the number of pants by the price of pants to find the total cost 
    totalCostOfShirts = (numShirts * shirtPrice); //This is multiplying the number of shirts by the price of shirts to find the total cost
    totalCostOfBelts = (numBelts * beltPrice);    //This is multiplying the number of belts by the price of belts to find the total cost
    salesTaxOnPants = (totalCostOfPants * paSalesTax);   //This is multiplying the cost of pants by tax to calculate the tax on pants
    salesTaxOnShirts = (totalCostOfShirts * paSalesTax); //This is multiplying the cost of shirts by tax to calculate the tax on shirts
    salesTaxOnBelts = (totalCostOfBelts * paSalesTax);   //This is multiplying the cost of belts by tax to calculate the tax on belts
    costOfPurchNoTax = (totalCostOfPants + totalCostOfShirts + totalCostOfBelts); //This adds the totals for shirts, pants, and belts...before tax
    totalAmountTaxed = (salesTaxOnPants + salesTaxOnShirts + salesTaxOnBelts);    //This adds the amount taxed for shirts, pants, and belts
    finalAmountPayed = (costOfPurchNoTax + totalAmountTaxed);   //This adds the total before tax and the total taxed in order to find the true total
    
    
    //It doesn't make sense to have more than 2 decimal places when talking about money, so this is me rounding the necessary values to 2 decimal places
    //I discover that I need to declare new variabeles in order to round, I do that here, Pt1 indicates Part 1, there will be a second step in rounding later
    double roundedTaxOnPantsPt1;       //Will be used to round the tax on pants
    double roundedTaxOnShirtsPt1;      //Will be used to round the tax on shirts
    double roundedTaxOnBeltsPt1;       //Will be used to round the tax on belts
    double roundedTaxTotalPt1;         //Will be used to round the total tax added
    double roundedTransactionTotalPt1; //Will be used to round the total cost of the transaction
    
    //I now perform the inital calculations needed to round the values
    roundedTaxOnPantsPt1 = (salesTaxOnPants * 100);         //This multiplies the tax on pants double by 100, this will be reversed later when I divide by 100
    roundedTaxOnShirtsPt1 = (salesTaxOnShirts * 100);       //This multiplies the tax on shirts double by 100, this will be reversed later when I divide by 100
    roundedTaxOnBeltsPt1 = (salesTaxOnBelts * 100);         //This multiplies the tax on belts double by 100, this will be reversed later when I divide by 100
    roundedTaxTotalPt1 = (totalAmountTaxed * 100);          //This multiplies the total taxed double by 100, this will be reversed later when I divide by 100
    roundedTransactionTotalPt1 = (finalAmountPayed * 100);  //This multiplies the final amount payed double by 100, this will be reversed later when I divide by 100
    
    //I need to turn the above calculations into integers to remove the decimal places, there is likely an easier way but I just declared a new Pt2(Part 2) variable
    int roundedTaxOnPantsPt2;         //Declares tax on pants to be an integer
    int roundedTaxOnShirtsPt2;        //Declares tax on shirts to be an integer 
    int roundedTaxOnBeltsPt2;         //Declares tax on belts to be an integer
    int roundedTaxTotalPt2;           //Decalres total tax to be an integer
    int roundedTransactionTotalPt2;   //Declares the total transaction to be an integer
    
    //I now undo the 100x multiplication I did above, I also have to force the Pt1 variables to be an integer, this is done with the extra (int) in front
    roundedTaxOnPantsPt2 = ((int) roundedTaxOnPantsPt1 / 100);              //Gives final integer answer for tax on pants
    roundedTaxOnShirtsPt2 = ((int) roundedTaxOnShirtsPt1 / 100);            //Gives final integer answer for tax on shirts
    roundedTaxOnBeltsPt2 = ((int) roundedTaxOnBeltsPt1 / 100);              //Gives final integer answer for tan on belts
    roundedTaxTotalPt2 = ((int) roundedTaxTotalPt1 / 100);                  //Gives final integer answer for total tax 
    roundedTransactionTotalPt2 = ((int) roundedTransactionTotalPt1 / 100);  //Gives final integer answer for the transaction total
    
    //This is me outputting various statements regarding total costs
    System.out.println("The total cost of pants is $" + totalCostOfPants); //This outputs the total cost of pants
    System.out.println("The total amount taxed on pants is $" + roundedTaxOnPantsPt2); //This outputs the total tax on pants
    System.out.println("The total cost of shirts is $" + totalCostOfShirts); //This outputs the total cost of shirts
    System.out.println("The total amount taxed on shirts is $" + roundedTaxOnShirtsPt2); //This outputs the total tax on shirts
    System.out.println("The total cost of belts is $" + totalCostOfBelts); //This outputs the total cost of belts
    System.out.println("The total amount taxed on belts is $" + roundedTaxOnBeltsPt2); //This outputs the total tax on belts
    System.out.println("The total cost of the purchase before tax is $" + costOfPurchNoTax); //This outputs the total cost of the purchase before tax
    System.out.println("The total amount added via tax is $" + roundedTaxTotalPt2); //This outputs the total cost of the tax
    System.out.println("The total amount charged in the transaction is $" + roundedTransactionTotalPt2); //This outputs how much you have to pay at the register
    
    } 
  }
//The final two brackets match up with the ones in the beginning, it closes the code and allows it to be compiled
    
    
    
    
    