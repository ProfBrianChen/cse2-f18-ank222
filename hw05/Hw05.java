//Andrew Kelly
//CSE02
//10/6/18

/*This program will run while statements and 
output number of loops and then
ouput probabilities of different poker hands */  
    
import java.util.Scanner;     //imports scanner
import java.lang.Math;        //imports math class for random variable

public class Hw05 {     //starts class hw05
  public static void main(String[] args) {    //starts main method
    
    Scanner myScanner = new Scanner(System.in);   //Declare scanner

System.out.println("Input the number of hands to generate. (in integer form)");   //Prompts user for number of hands(loops)
    int onePairCounter = 0;                         //declares and initializes the counter for one pair
    double numHands = myScanner.nextInt();             //calls number of hands through a  scanner
    for(int i = 1; i <= numHands; i++){             //for statement, says the variable i starts at 1 and ends at numHands, and that it increments each time until that
      
      int card1 = (int)(Math.random()*52)+1;        //declares card1 to be an integer value 1-52
      int card2 = (int)(Math.random()*52)+1;        //declares card2 to be an integer value 1-52
      int card3= (int)(Math.random()*52)+1;         //declares card3 to be an integer value 1-52
      int card4= (int)(Math.random()*52)+1;         //declares card4 to be an integer value 1-52
      int card5= (int)(Math.random()*52)+1;         //declares card5 to be an integer value 1-52
     
      while (card2 == card1){                       //While statement      **********  
        card2 = (int)(Math.random()*(52))+1;
      } 
      
      card1 = card1 %13;                              //Uses modulus to take into account the pattern of 13 for card1
     // System.out.println("card 1 = " + card1);        //Outputs what card1 is
      card2 = card2 %13;                              //Uses modulus to take into account the pattern of 13 for card2
    //  System.out.println("card 2= " + card2);         //Outputs what card2 is
     card3 = card3 %13;                               //Uses modulus to take into account the pattern of 13 for card3
    //  System.out.println("card 3= " + card3);         //Outputs what card3 is
      card4 = card4 %13;                              //Uses modulus to take into account the pattern of 13 for card4
     // System.out.println("card 4= " + card4);          //Outputs what card4 is
      card5 = card5 %13;                              //Uses modulus to take into account the pattern of 13 for card5
     // System.out.println("card 5= " + card5);          //Outputs what card5 is

      //Below is the if statement that takes into account all of the times when there would be a one pair
       if ((card1 == card2) || (card1 == card3) || (card1 == card4) || (card1 == card5) || (card2 == card3) || (card2 == card4) || (card2 == card5) || (card3 == card4) || (card3 == card5) || (card4 == card5)){
        onePairCounter++;       //This is activated if the if statement is true, it ups the counter by 1
      }                         //closes the if statement for one pair
          
    }   //closes the for statement for one pair
   // System.out.println("There are " + onePairCounter + " one pairs.");     //outputs how many one pairs were found
    
    
    int twoPairCounter = 0;                           //declares and initializes the counter for two pairs
   
    for (int x = 1; x <= numHands; x++){              //for statement, says the variable x starts at 1 and ends at numHands, and that it increments each time until that
      int card1 = (int)(Math.random()*52)+1;        //declares card1 to be an integer value 1-52
      int card2 = (int)(Math.random()*52)+1;        //declares card2 to be an integer value 1-52
      int card3= (int)(Math.random()*52)+1;         //declares card3 to be an integer value 1-52
      int card4= (int)(Math.random()*52)+1;         //declares card4 to be an integer value 1-52
      int card5= (int)(Math.random()*52)+1;         //declares card5 to be an integer value 1-52
      
      card1 = card1 %13;                              //Uses modulus to take into account the pattern of 13 for card1
    //  System.out.println("card 1 = " + card1);        //Outputs what card1 is
      card2 = card2 %13;                              //Uses modulus to take into account the pattern of 13 for card2
    //  System.out.println("card 2= " + card2);         //Outputs what card2 is
      card3 = card3 %13;                               //Uses modulus to take into account the pattern of 13 for card3
     // System.out.println("card 3= " + card3);         //Outputs what card3 is
      card4 = card4 %13;                              //Uses modulus to take into account the pattern of 13 for card4
     // System.out.println("card 4= " + card4);          //Outputs what card4 is
      card5 = card5 %13;                              //Uses modulus to take into account the pattern of 13 for card5
     // System.out.println("card 5= " + card5);          //Outputs what card5 is
                    
      //Below is the if statement for every combination of two pairs
      if (((card1 == card2) && (card3 == card4)) || ((card1 == card3) && (card2 == card4)) || ((card1 == card5) && (card2 == card3)) || ((card1 == card5) && (card2 == card4)) || ((card1 ==card2) && (card3 == card5)) ||(( card1 == card2) && (card4 == card5)) || ((card1 == card3) && (card2 == card5)) || ((card1 == card3) && (card4 == card5)) || ((card1 == card5) && (card3 == card4)) || ((card1 == card4) && (card2 == card3)) || ((card1 == card4) && (card2 == card5)) || ((card1 == card4) && (card3 == card5))){
        twoPairCounter++;     //increases the two pair counter by 1 if the if statement is true
      }                       //closes the two pair if statement
         
    }                         //closes the two pair for statement
    // System.out.println("There are " + twoPairCounter + " two pairs.");       //outputs how many two pairs were found
    
    
    int threeKindCounter = 0;                             //declares and initializes the counter for three of a kind
    
    for (int y = 1; y <=numHands; y++){                   //for statement, says the variable y starts at 1 and ends at numHands, and that it increments each time until that
      int card1 = (int)(Math.random()*52)+1;        //declares card1 to be an integer value 1-52
      int card2 = (int)(Math.random()*52)+1;        //declares card2 to be an integer value 1-52
      int card3= (int)(Math.random()*52)+1;         //declares card3 to be an integer value 1-52
      int card4= (int)(Math.random()*52)+1;         //declares card4 to be an integer value 1-52
      int card5= (int)(Math.random()*52)+1;         //declares card5 to be an integer value 1-52
      card1 = card1 %13;                              //Uses modulus to take into account the pattern of 13 for card1
     // System.out.println("card 1 = " + card1);        //Outputs what card1 is
      card2 = card2 %13;                              //Uses modulus to take into account the pattern of 13 for card2
    //  System.out.println("card 2= " + card2);         //Outputs what card2 is
      card3 = card3 %13;                               //Uses modulus to take into account the pattern of 13 for card3
     // System.out.println("card 3= " + card3);         //Outputs what card3 is
      card4 = card4 %13;                              //Uses modulus to take into account the pattern of 13 for card4
     // System.out.println("card 4= " + card4);          //Outputs what card4 is
      card5 = card5 %13;                              //Uses modulus to take into account the pattern of 13 for card5
     // System.out.println("card 5= " + card5);          //Outputs what card5 is
      
      //Below is the if statement for every possible combination of three of a kind
      if( ((card1 == card2) && (card2 ==card3)) || ((card1 == card2) && (card2 == card4)) || ((card1 == card2) &&(card2 == card5)) || ((card2 == card3) && (card3 == card4)) || ((card2 == card3) && (card3 == card4)) ||( (card2 == card4) && (card4 == card5)) || ((card3== card4) &&(card4 == card5))) {
        threeKindCounter++;
    }       //closes the three of a kind if statement
  }         //closes the three of a kind for statement
  //  System.out.println("There are " + threeKindCounter + " three kinds.");        //outputs how many three of a kinds were found
    
    
    int fourKindCounter =0;                                   //declares and initializes the counter for four of a kind
    
    for (int f = 1; f <=numHands; f++){                       //for statement, says the variable f starts at 1 and ends at numHands, and that it increments each time until that
      int card1 = (int)(Math.random()*52)+1;        //declares card1 to be an integer value 1-52
      int card2 = (int)(Math.random()*52)+1;        //declares card2 to be an integer value 1-52
      int card3= (int)(Math.random()*52)+1;         //declares card3 to be an integer value 1-52
      int card4= (int)(Math.random()*52)+1;         //declares card4 to be an integer value 1-52
      int card5= (int)(Math.random()*52)+1;         //declares card5 to be an integer value 1-52
    
      card1 = card1 %13;                              //Uses modulus to take into account the pattern of 13 for card1
      //System.out.println("card 1 = " + card1);        //Outputs what card1 is
      card2 = card2 %13;                              //Uses modulus to take into account the pattern of 13 for card2
      //System.out.println("card 2= " + card2);         //Outputs what card2 is
      card3 = card3 %13;                               //Uses modulus to take into account the pattern of 13 for card3
     // System.out.println("card 3= " + card3);         //Outputs what card3 is
      card4 = card4 %13;                              //Uses modulus to take into account the pattern of 13 for card4
     // System.out.println("card 4= " + card4);          //Outputs what card4 is
      card5 = card5 %13;                              //Uses modulus to take into account the pattern of 13 for card5
     // System.out.println("card 5= " + card5);          //Outputs what card5 is
    
      //Below is the if statement for ever possible four of a kind
    if (((card1 == card2) && (card2 == card3) && (card3 == card4)) || ((card1 == card2) && (card2 == card3) && (card3 == card5)) || ((card1 == card2) && (card2 == card4) && (card4 == card5)) || ((card1 == card3) && (card3 == card4) && (card4== card5)) || ((card2  == card3) && (card3 == card4) && (card4 == card5))){
      fourKindCounter++;
    }       //closes the if statement for four of a kind
    }       //closes the for statement for four of a kind
   // System.out.println("There are " + fourKindCounter + " four kinds.");      //Outputs how many four of a kinds were found
    
    
    double probabilityOnePair;      //declares and initializes the probability of getting one pair
    double probabilityTwoPair;      //declares and initializes the probability of getting two pair
    double probabilityThreeKind;    //declares and initializes the probability of getting three of a kind
    double probabilityFourKind;     //declares and initializes the probability of getting four of a kind
    
    probabilityOnePair = ((onePairCounter/numHands)*100);        //Calculates the probability of getting one pair by dividing the counter by the total number of loops, then multiplies by 100 to put into percent form
    probabilityTwoPair = ((twoPairCounter/numHands)*100);        //Calculates the probability of getting two pair by dividing the counter by the total number of loops, then multiplies by 100 to put into percent form
    probabilityThreeKind = ((threeKindCounter/numHands)*100);    //Calculates the probability of getting three of a kind by dividing the counter by the total number of loops, then multiplies by 100 to put into percent form
    probabilityFourKind = ((fourKindCounter/numHands)*100);      //Calculates the probability of getting four of a kind by dividing the counter by the total number of loops, then multiplies by 100 to put into percent form
    
    System.out.println("The number of loops is: " + numHands);                                       //Outputs the total number of loops(user inputted)
    System.out.printf("The probability of a One-pair is: %.3f", probabilityOnePair);             //Outputs the probability of getting a one pair
    System.out.println("%");                                                                     //Outputs a % at the end of the previous line
    System.out.printf("The probability of a Two-pair is: %.3f", probabilityTwoPair);             //Outputs the probability of getting a two pair
    System.out.println("%");                                                                     //Outputs a % at the end of the previous line
    System.out.printf("The probability of a Three-of-a-kind is: %.3f", probabilityThreeKind);    //Outputs the probability of getting a three of a kind
    System.out.println("%");                                                                     //Outputs a % at the end of the previous line
    System.out.printf("The probability of a Four-of-a-kind is: %.3f", probabilityFourKind);      //Outputs the probability of getting a four of a kind
    System.out.println("%");                                                                     //Outputs a % at the end of the previous line
    
  }   //ends main method
}     //ends class and code
