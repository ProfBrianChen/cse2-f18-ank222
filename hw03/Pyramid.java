//Andrew Kelly
//CSE 02
//9/17/18

// This program will: prompt the user for the dimensions of a pyramind and then output the volume of the pyramind


import java.util.Scanner;   //Imports the scanner needed

public class Pyramid {      //This introduces the class
  
  public static void main(String[] args) {    //This is the main method used in every Java program
    
    Scanner myScanner = new Scanner(System.in);   //This declares the scanner
    
    System.out.print("The square side of the pyramid is (input length) : ");    //Outputs whats in "", no ln means line 17 will accompany it
    int squareSideLength = myScanner.nextInt();   //This accepts user input for line 16, declares length to be an integer
    
    System.out.print("The height of the pyramid is (input height) : ");   //Outputs whats in "", no ln means line 20 will accompany it
    int pyramidHeight = myScanner.nextInt();    //This accepts user input for line 19, declars height to be an integer
        
    //Below I do coding neccesary to calculate the volume of the pyramid
    int pyramidVolume;    //declares pyramid volume to be an integer
    pyramidVolume = (((squareSideLength * squareSideLength) * pyramidHeight) / 3);    //Performs the calculation needed for volume of a pyramidVolume
    
    System.out.print("The volume inside the pyramid is: " + pyramidVolume + ".");   //Displays the statement and the calculated volume
    
  }   //This ends the main method
}   //This ends the class
