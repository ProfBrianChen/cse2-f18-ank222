//Andrew Kelly
//CSE 002
//9/16/18


/*This program is designed to allow the user to input values for:
acres of land affected by hurricane precipitation
and how many inches of rain were dropped on average
the quantity of rain will then be converted to cubic miles*/

import java.util.Scanner;     //This imports the scanner

public class Convert {      //This introduces the class
  
  public static void main(String[] args) {    //This is the main method, used in every java program
    
    Scanner myScanner = new Scanner(System.in);   //This declares the scanner
    
    System.out.print("Enter the affected area in acres: ");     //This outputs the statement in "", since no ln, line 20 will not drop to a new line
    double areaAcre = myScanner.nextDouble();   //This accepts user input for line 19
    
    System.out.print("Enter the rainfall in the affect area: ");    //This outputs the statement in "", no ln means line 23 will be outputted with line 22
    double rainInches = myScanner.nextDouble();   //This accepts user input for line 22
    
    //Below is my process to convert acre-inches to cubic miles
    double gallons;   //declares gallons to be a variable and a double
    gallons = (areaAcre * 27154.28599076099 * rainInches);   //This converts acre-inches to gallons
    double cubicMiles;   //declares cubic miles to be a double
    cubicMiles = (gallons * .00000000000090187);    //This converts gallons to cubic miles
    
    System.out.print(cubicMiles + " cubic miles");   //This outputs the number of cubic miles
    
  }   //This ends the main method
}   //This ends the class