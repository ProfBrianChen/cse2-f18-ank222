//Andrew Kelly
//CSE02
//10/5/18

/* This lab will ask the user for multiple types of input,
if the user inputs the incorrect type, the code will tell 
the user they were incorrect and loop back*/

import java.util.Scanner;      //Imports scanner

public class userCourseInfo {      //Starts the class userCourseInfo
  public static void main(String[] args) {    //Starts the main method
    
    Scanner myScanner = new Scanner(System.in);   //Declare scanner
    boolean continueLoop = true;      //Declare continueLoop to be a true boolean
    boolean correctInt;               //Declares boolean integer correctInt
    boolean correctString;            //Declares boolean string correctString
    
    //The next 2 lines welcome the user
    System.out.println("Welcome to the code!");     
    System.out.println("You will be prompted to enter lots of information about your class, get ready!");
    
    System.out.println("First, enter the course number in integer form");   //Asks user for course number
        
    while (continueLoop){       //Starts while, will continue loop when true
      correctInt = myScanner.hasNextInt();      //runs the scanner and checks if the user entered an integer
      if (correctInt){    //Within the while loop, if statement to see if correctInt is true
        int x = myScanner.nextInt();        //takes the integer x into the scanner for user input
        System.out.printf("You entered the course number: %d.\n", x);   //Outputs the entered value
        continueLoop = !continueLoop;        //Switches continueLoop to false so it won't keep going through the while
      }   //closes if for correctInt
      else{     //Starts the else statement that matches the if statement, inside of the while
        myScanner.next();   //Takes user input
        System.out.println("You have not entered an invalid value, an integer was required");   //Tells user it entered the wrong type
      }     //Closes the else
    }     //Closes while loop for course number
    
    continueLoop = !continueLoop;        //Turns the continueLoop back to true for the next while

    //All of the other inputs follow the same pattern with variations in type, comments are the same as well
    System.out.println("Next, enter the department name in string form");
        
    while (continueLoop){
      correctString = myScanner.hasNext();
      if (correctString){
        String x = myScanner.next();
        System.out.println("You entered the department name: " + x);
        continueLoop = !continueLoop;        
      }   //closes if for correctString
      else{
        myScanner.next();
        System.out.println("You have not entered an invalid value, a string was required");
      }     //Closes the else
    }     //Closes while loop for department
    
    
    continueLoop = !continueLoop;        //Turns the continueLoop back to true

    System.out.println("Next, enter the number of times per week the class meets, in integer form");
        
    while (continueLoop){
      correctInt = myScanner.hasNextInt();
      if (correctInt){
        int x = myScanner.nextInt();
        System.out.println("You entered the amount: " + x + " classes per week.");
        continueLoop = !continueLoop;        
      }   //closes if for correctString
      else{
        myScanner.next();
        System.out.println("You have not entered an invalid value, an integer was required");
      }     //Closes the else
    }     //Closes while loop for number classes per week
    
    
    continueLoop = !continueLoop;        //Turns the continueLoop back to true

    System.out.println("Next, enter the time the class starts, in military time. This will be an integer 'xxxx'");
        
    while (continueLoop){
      correctInt = myScanner.hasNextInt();
      if (correctInt){
        int x = myScanner.nextInt();
        System.out.println("You entered the time: " + x);
        continueLoop = !continueLoop;        
      }   //closes if for correctInt
      else{
        myScanner.next();
        System.out.println("You have not entered an invalid value, an integer was required");
      }     //Closes the else
    }     //Closes while loop for class time
    
    
    continueLoop = !continueLoop;        //Turns the continueLoop back to true

    System.out.println("Next, enter the instructors name in string form");
        
    while (continueLoop){
      correctString = myScanner.hasNext();
      if (correctString){
        String x = myScanner.next();
        System.out.println("You entered the instructor name: " + x);
        continueLoop = !continueLoop;        
      }   //closes if for correctString
      else{
        myScanner.next();
        System.out.println("You have not entered an invalid value, a string was required");
      }     //Closes the else
    }     //Closes while loop for department
    
    
    continueLoop = !continueLoop;        //Turns the continueLoop back to true

    System.out.println("Next, enter the number of students in your class");
        
    while (continueLoop){
      correctInt = myScanner.hasNextInt();
      if (correctInt){
        int x = myScanner.nextInt();
        System.out.println("You entered: " + x + " number of students.");
        continueLoop = !continueLoop;        
      }   //closes if for correctInt
      else{
        myScanner.next();
        System.out.println("You have not entered an invalid value, an integer was required");
      }     //Closes the else
    }     //Closes while loop for number of students
    
    System.out.println("Thank you for entering your course information");
    
    
  }   //Closes the main method
}   //Closes the class
