//Andrew Kelly
//CSE02
//10.20.18

//This code will 'hide' an X within a box of stars
//The size of the box will be based on user input

import java.util.Scanner;     //imports scanner

public class EncryptedX {                     //Starts class EncrpytedX
  public static void main(String [] args){    //Starts the main method, needed for every java
    
   Scanner myScanner = new Scanner(System.in);   //Declare scanner
   System.out.println("Hello, welcome to the code!");   //Welcomes user
   System.out.println("Please enter an integer value from 0-100 that will serve as the length of your box.");     //Prompts user to enter integer
    
    //lines 18-28 check if the user entered an integer value from 0-100, if not, they are reprompted
    boolean continueLoop = myScanner.hasNextInt();      //creates boolean continueLoop, 23-28 check if integer
    while (!continueLoop){    //starts while for when continueloop is false
      myScanner.next();       //calls scanner
      System.out.println("You have not entered an integer, please try again.");    //Outputs that the user did not enter integer
      continueLoop = myScanner.hasNextInt();      //sends user back through
    }     //closes while for integer check
    
    int userInt = myScanner.nextInt();      //declares int userInt through scanner, lines 30-34 deal with checking range
    while ((userInt < 0) || (userInt > 100)){    //while loop and checks range
      System.out.println("You have not entered a value within 0-10, please try again.");    //outputs that the user did not enter within range
      userInt = myScanner.nextInt();      //sends user back through
    }     //closes while for range check
    int counter = 0;    //declares counter, will be used for the x
    for (int numRows = 0; numRows < userInt; numRows++){    //outer for loop, deals with numrows and increments
      for (int numCollumns = 0; numCollumns < userInt; numCollumns++){    //inner for loop, deals with numcollumns and increments
        counter++;    //increments the counter everytime the inner for runs
        if(((userInt - (counter - 1)) == numRows )|| ((counter - 1) == numRows)){   //if statement within the inner for, sets condition for when the X space occurs
          System.out.print(" ");      //Outputs the spaces that will make up the X
        }   //closes the if statement
        System.out.print("*");    //Outputs a *, without going to a new line
      }     //closes the inner for loop
      counter = 0;    //sets the counter to 0
      System.out.println("");   //Outputs the new line
      }   //closes the outer for loop
    
  }   //ends the main method
}     //ends the class and the code