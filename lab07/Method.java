//Andrew Kelly
//CSE02
//10.26.18

/*This lab will output sample sentences based on a user inputted integer
the code will run multiple methods to accomplish this*/

import java.util.*; //imports random number
//import java.util.Scanner; //imports scanner

public class Method { // starts class

	public static void main(String[] args) { // starts main
		System.out.println("Welcome to the program, here is your first sentence."); // greets user
		System.out.println(Sentence()); // calls the sentence method
		Paragraph(); // calls the paragraph method
	} // ends main

	public static String Sentence() { // starts sentence method
		String sentence = ""; // declares and initializes the sentence variable
		sentence = "The " + Adjective() + " " + Noun1() + " " + Verb() + " the " + Noun2() + "."; // assigns desired output to sentence
		return sentence; // returns sentence
	} // ends sentence method

	public static void Paragraph() { // starts the paragraph method
		boolean continueLoop = true; // declares and sets boolean to true
		Scanner myScanner = new Scanner(System.in); // declares scanner myScanner
		String para = ""; // declares and initializes string para

		while (continueLoop) { // starts while for continueLoop
			System.out.println("Would you like another sentence to form a paragraph, yes or no?"); // Asks user if theyd like a sentence
			String answer = myScanner.next(); // accepts user input

			if (answer.equals("no")) { // if statement to see if user said no
				continueLoop = false; // sets continueloop to false if user said no
				System.out.println("Okay, goodbye."); // says goodbye to user
			} // closes if
			else if (answer.equals("yes")) { // else if statment for when user says yes
				para += Sentence() + " "; // amends next sentence to the prior
				System.out.println(para); // outputs the para
			} // closes else if
			else { // else statement for if user enters something other than yes or no
				System.out.println("You entered an invalid word, please enter yes or no."); // reprompts user to enter
																							// yes or no
			} // closes else

		} // closes while

	} // closes paragraph method

	public static String Adjective() { // starts adjective method
		Random randomGenerator = new Random(); // declares random generator
		String randAdj = ""; // declares and initializes randAdj
		int randomInt = randomGenerator.nextInt(10); // calls random integer
		switch (randomInt) { // starts switch for randomInt to delcare adjs
		case 0: // case 0, random digit is 0
			randAdj = "big"; // assigns big to randAdj when random value is 0
			break; // breaks case 0
		// this pattern of commenting continues for the rest of this method, and the
		// other methods below
		case 1:
			randAdj = "small";
			break;
		case 2:
			randAdj = "dumb";
			break;
		case 3:
			randAdj = "smart";
			break;
		case 4:
			randAdj = "fast";
			break;
		case 5:
			randAdj = "slow";
			break;
		case 6:
			randAdj = "kind";
			break;
		case 7:
			randAdj = "mean";
		case 8:
			randAdj = "funny";
			break;
		case 9:
			randAdj = "crazy";
			break;
		default: // default statement needed for switch
			break; // since switch uses only random digits from 0-9, doesnt do anything
		} // ends switch
		return randAdj; // returns the randAdj assigned in the switch
	} // ends adjective method

	public static String Noun1() { // starts noun1 method
		Random randomGenerator = new Random();
		String randNoun1 = "";
		int randomInt = randomGenerator.nextInt(10);
		switch (randomInt) {
		case 0:
			randNoun1 = "alpaca";
			break;
		case 1:
			randNoun1 = "armadillo";
			break;
		case 2:
			randNoun1 = "bobcat";
			break;
		case 3:
			randNoun1 = "cat";
			break;
		case 4:
			randNoun1 = "catfish";
			break;
		case 5:
			randNoun1 = "dog";
			break;
		case 6:
			randNoun1 = "elephant";
			break;
		case 7:
			randNoun1 = "flamingo";
		case 8:
			randNoun1 = "fox";
			break;
		case 9:
			randNoun1 = "ferret";
			break;
		default:
			break;
		} // ends switch
		return randNoun1; // returns randNoun1
	} // ends noun1 method

	public static String Verb() { // creates verb method
		Random randomGenerator = new Random();
		String randVerb = "";
		int randomInt = randomGenerator.nextInt(10);
		switch (randomInt) {
		case 0:
			randVerb = "killed";
			break;
		case 1:
			randVerb = "talked to";
			break;
		case 2:
			randVerb = "ate";
			break;
		case 3:
			randVerb = "thanked";
			break;
		case 4:
			randVerb = "welcomed";
			break;
		case 5:
			randVerb = "talked to";
			break;
		case 6:
			randVerb = "played with";
			break;
		case 7:
			randVerb = "yelled at";
		case 8:
			randVerb = "hugged";
			break;
		case 9:
			randVerb = "kissed";
			break;
		default:
			break;
		} // ends switch
		return randVerb; // returns randVerb
	} // ends verb method

	public static String Noun2() { // starts noun2
		Random randomGenerator = new Random();
		String randNoun2 = "";
		int randomInt = randomGenerator.nextInt(10);
		switch (randomInt) {
		case 0:
			randNoun2 = "goose";
			break;
		case 1:
			randNoun2 = "hen";
			break;
		case 2:
			randNoun2 = "iguana";
			break;
		case 3:
			randNoun2 = "jaguar";
			break;
		case 4:
			randNoun2 = "llama";
			break;
		case 5:
			randNoun2 = "monkey";
			break;
		case 6:
			randNoun2 = "parrot";
			break;
		case 7:
			randNoun2 = "rhino";
		case 8:
			randNoun2 = "salamander";
			break;
		case 9:
			randNoun2 = "snake";
			break;
		default:
			break;
		} // ends switch
		return randNoun2; // returns noun2
	} // ends noun2 method

} // ends class and code